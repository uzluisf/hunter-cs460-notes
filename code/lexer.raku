#!/bin/env raku

#-------------------------------------------------------------------------------
# A lexical analyzer system for simple arithmetic expressions implemented in
# Raku and based on Sebesta's "Concepts of Programming Languages" (front.c). It
# has minor changes, however the functionality is still the same: go through a
# string representing a simple arithmetic expression and classify its lexemes.
# 
# $ raku lexer.raku '(sum + 47) / total'
#  Next Token      Next Lexeme
# ---------------------------------
#  LEFT_PAREN      (
#  IDENTIFIER      sum
#  ADD_OP          +
#  INT_LIT         47
#  RIGHT_PAREN     )
#  DIV_OP          /
#  IDENTIFIER      total
#  EOL             EOL
#-------------------------------------------------------------------------------

#
# Types
#
enum CharClass(LETTER => 0, DIGIT => 1, UNKNOWN => 99, EOL => 100);

enum Token(
    INT_LIT => 10, IDENTIFIER => 11, ASSIGN_OP   => 20,
    ADD_OP  => 21, SUB_OP     => 22, MULT_OP     => 23,
    DIV_OP  => 24, LEFT_PAREN => 25, RIGHT_PAREN => 26,
);

subset Char of Str where *.chars == 1;

#
# File-scoped variables
#
my Char          @lexeme;    # list of characters forming a lexeme
my Char          $nextChar;  # next character
my CharClass     $charClass; # character class (LETTER, DIGIT, etc.)
my               $nextToken; # lexeme's categorization
my IO::CatHandle $fh;        # file handle

#
# Functions
#

#| Add next character to lexeme.
sub addChar(--> Nil) {
    @lexeme.push($nextChar)
}

# Get next character and determine its character class.
sub getChar(--> Nil) {
    if $fh.eof {
        $charClass = EOL;
    }
    else {
        $nextChar = $fh.readchars(1);
        $charClass = do given $nextChar {
            when .match(/<alpha>/) { LETTER  }
            when .match(/<digit>/) { DIGIT   }
            default                { UNKNOWN }
        }
    }
}

#| Remove all whitespace by calling getChar until
#| $nextChar stores a non-whitespace character.
sub getNonBlank(--> Nil) {
    getChar until $nextChar ne ' '
}

#| Look up operators and parentheses, and return the token.
sub lookup($ch) {
    addChar;
    $nextToken = do given $ch {
        when '(' { LEFT_PAREN  }
        when ')' { RIGHT_PAREN }
        when '+' { ADD_OP      }
        when '-' { SUB_OP      }
        when '*' { MULT_OP     }
        when '/' { DIV_OP      }
        default  { EOL         }
    }

    return $nextToken;
}

#| Simple lexical analyzer for arithmetic expressions.
sub lex {
    getNonBlank;

    given $charClass {
        # parse identifiers
        when LETTER {
            addChar;
            getChar;
            while $charClass == LETTER or $charClass == DIGIT {
                addChar;
                getChar;
            }

            $nextToken = IDENTIFIER;
        }

        # parse integer literals
        when DIGIT {
            addChar;
            getChar;
            while $charClass == DIGIT {
                addChar;
                getChar;
            }

            $nextToken = INT_LIT;
        }

        # parentheses and operators
        when UNKNOWN {
            lookup $nextChar;
            getChar;
        }

        when EOL {
            $nextToken = EOL;
            @lexeme = <E O L>;
        }
    }

    printf " %-15s %-15s\n", $nextToken, @lexeme.join;

    # empty it and prepare for next lexeme.
    @lexeme = Empty;
}

#
# Main driver
#

sub MAIN(Str:D $expr) {
    (my $temp = 'expr.txt'.IO).spurt($expr);
    $fh = IO::CatHandle.new($temp);
   
    my $s = sprintf " %-15s %-15s\n", 'Next Token', 'Next Lexeme';
    say $s, '-' x $s.chars;

    getChar;
    $nextToken = UNKNOWN;
    lex until $nextToken == EOL;
}
