-- 06. Recursion

{-
1. How does the recursive version of the factorial function behave if applied to
   a negative argument, such as (-1)? Modify the definition to prohibit negative
   arguments by adding a guard to the recursive case.

   The function keeps recursing because the base case is never reached.
-}

fact :: Int -> Int
fact n
    | n == 0 = 1
    | n >= 1 = n * fact(n-1)

{-
2. Define a recursive function sumdown :: Int -> Int that returns the sum of the
   non-negative integers from a given value down to zero. For example, sumdown 3
   should return the result 3+2+1+0 = 6.
-}

sumdown :: Int -> Int
sumdown n
    | n == 0 = 0
    | n >= 1 = n + sumdown(n-1)

{-
3. Define the exponentiation operator ^ for non-negative integers using the same
   pattern of recursion as the multiplication operator *, and show how the ex-
   pression 2 ^ 3 is evaluated using your definition.

   exp' 2 3
   = 2 * (exp' 2 2)
   = 2 * (2 * (exp' 2 1))
   = 2 * (2 * (2 * (exp' 2 0))
   = 2 * (2 * (2 * (1))
   = 2 * (2 * 2)
   = 2 * 4
   = 8
-}

exp' :: Int -> Int -> Int
exp' b e
    | e == 0 = 1
    | e >= 1 = b * exp' b (e-1)

{-
4. Define a recursive function euclid :: Int -> Int -> Int that implements
   Euclid’s algorithm for calculating the greatest common divisor of two non-
   negative integers: if the two numbers are equal, this number is the result;
   otherwise, the smaller number is subtracted from the larger, and the same
   process is then repeated. For example:

   > euclid 6 27
   3
-}

euclid :: Int -> Int -> Int
euclid n m
    | n == m    = n
    | otherwise = if n < m
                  then euclid (m-n) n
                  else euclid (n-m) m

{-
5. Using the recursive definitions given in this chapter, show how length
   [1,2,3], drop 3 [1,2,3,4,5], and init [1,2,3] are evaluated.

    length' [] = 0
    length' (_:xs) = 1 + length' xs

    length' [1, 2, 3]
    = 1 + length' [2, 3]
    = 1 + 1 + length' [3]
    = 1 + 1 + 1 + length' [0]
    = 1 + 1 + 1 + 0
    = 3

    drop' :: Int -> [a] -> [a]
    drop' 0 xs = xs
    drop' _ [] = []
    drop' n (_:xs) = drop' (n-1) xs

    drop' 3 [1,2,3,4,5]
    = drop' 2 [2,3,4,5]
    = drop' 1 [3,4,5]
    = drop' 0 [4,5]
    = [4,5]

    init' :: [a] -> [a]
    init' [_] = []
    init' (x:xs) = x : init' xs

    init' [1,2,3]
    = 1 : init' [2,3]
    = 1 : 2 : init' [3]
    = 1 : 2 : []
    = [1,2]
-}


{-
6. Without looking at the definitions from the standard prelude, define the
   following library functions on lists using recursion.

   a. Decide if all logical values in a list are True:

        and :: [Bool] -> Bool

    b. Concatenate a list of lists:
        concat :: [[a]] -> [a]

    c. Produce a list with n identical elements:
        replicate :: Int -> a -> [a]

    d. Select the nth element of a list:
        (!!) :: [a] -> Int -> a

    e. Decide if a value is an element of a list:
        elem :: Eq a => a -> [a] -> Bool
-}

-- 6.a
and' :: [Bool] -> Bool
and' []    = True
and' (x:xs) = x && and' xs

-- 6.b
concat' :: [[a]] -> [a]
concat' []     = []
concat' (x:xs) = x ++ concat' xs

-- 6.c
replicate' :: Int -> a -> [a]
replicate' 0 x = []
replicate' n x = x : replicate' (n-1) x

-- 6.d
selectn :: [a] -> Int -> a
selectn (x:xs) n
    | n == 0    = x
    | otherwise = selectn xs (n-1)

-- 6.e
elem' :: Eq a => a -> [a] -> Bool
elem' y  []     = False
elem' y (x:xs)
    | y == x    = True
    | otherwise = elem' y xs

{-
7. Define a recursive function merge :: Ord a => [a] -> [a] -> [a] that merges
   two sorted lists to give a single sorted list. For example: 

   > merge [2,5,6] [1,3,4]
   [1,2,3,4,5,6]

   Note: your definition should not use other functions on sorted lists such as
   insert or isort, but should be defined using explicit recursion.
-}

merge :: Ord a => [a] -> [a] -> [a] 
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys) 
    | x <= y    = x : merge xs (y:ys)
    | otherwise = y : merge (x:xs) ys

{-
8. Using merge, define a function msort :: Ord a => [a] -> [a] that implements
  merge sort , in which the empty list and singleton lists are already sorted, and
  any other list is sorted by merging together the two lists that result from
  sorting the two halves of the list separately. 

  Hint: first define a function halve :: [a] -> ([a],[a]) that splits a list into
  two halves whose lengths differ by at most one.
-}

halve :: [a] -> ([a],[a]) 
halve xs = (take n xs, drop n xs)
  where
    n = (length xs) `div` 2

msort :: Ord a => [a] -> [a] 
msort []  = []
msort [x] = [x]
msort xs  = merge (msort (fst (halve xs))) (msort (snd (halve xs)))

{-
9. Using the five-step process, construct the library functions that:

    a. calculate the sum of a list of numbers;
    b. take a given number of elements from the start of a list;
    c. select the last element of a non-empty list.
-}

{-
a. calculate the sum of a list of numbers;

1) define the type
sum' :: [Int] -> Int

2) enumerate the cases

sum' []     =
sum' (x:xs) =

3) define the simple cases

sum' []     = 0
sum' (x:xs) =

4) define the other cases

sum' []     = 0
sum' (x:xs) = x + sum' xs

5) generalize and simplify 

sum' :: Num a => [a] -> a
sum' []     = 0
sum' (x:xs) = x + sum' xs

Or using one of the folding functions:

sum' :: Num a => [a] -> a
sum' = foldr (*) 0
-}


{-
b. take a given number of elements from the start of a list;

1)

take' :: Int -> [Int] -> [Int]

2)

take' _ [] = 
take' 0 xs =
take' n (x:xs) = 

3)

take' 0 xs     = []
take' n []     = []
take' n (x:xs) = 

4) 

take' 0 xs     = []
take' n []     = []
take' n (x:xs) = x : take' (n-1) xs

5)

take' :: Int -> [a] -> [a]
take' 0 xs     = []
take' n []     = []
take' n (x:xs) = x : take' (n-1) xs

-}

{-
c. select the last element of a non-empty list.

1)

last' :: [a] -> a

2) 

last' [x]    = 
last' (x:xs) =

3) 

last' [x]    = x
last' (x:xs) =

4)

last' [x]    = x
last' (x:xs) = last' xs

5)

last' :: [a] -> a
last' [x]    = x
last' (_:xs) = last' xs

-}

