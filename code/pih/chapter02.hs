-- 02. First Steps

{-
1. Work through the examples from this chapter using GHCi.
-}

{-
2. Parenthesise the following numeric expressions:

    2^3*4   = (2^3) * 4
    2*3+4*5 = (2*3) + (4*5)
    2+3*4^5 = 2 + (3 * (4^5))
-}

{-
3. The script below contains three syntactic errors. Correct these errors and
   then check that your script works properly using GHCi.
-}

n = 
    a `div` (length xs)
  where
    a = 10
    xs = [1,2,3,4,5]

{-
4. The library function last selects the last element of a non-empty list; for
   example, last [1,2,3,4,5] = 5. Show how the function last could be defined
   in terms of the other library functions introduced in this chapter. Can you
   think of another possible definition?
-}

lasta    :: [a] -> a
lasta xs = 
    head (drop n xs)
  where
    n = (length xs) - 1

lastb    :: [a] -> a
lastb xs = head (reverse xs)

{-
5. The library function init removes the last element from a non-empty list; for
   example, init [1,2,3,4,5] = [1,2,3,4]. Show how init could similarly be
   defined in two different ways.
-}

inita :: [a] -> [a]
inita xs = reverse (drop 1 (reverse xs))

initb :: [a] -> [a]
initb (x:xs)
    | null xs   = []
    | otherwise = x : (initb xs)
