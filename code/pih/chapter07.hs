-- 07. Higher-order functions

import Data.Char

{-
1. Show how the list comprehension [f x | x <- xs, p x] can be re-expressed
   using the higher-order functions map and filter.
-}

mapfilter f p = map f . filter p

sqrevens = mapfilter (^2) even

{-
2. Without looking at the definitions from the standard prelude, define the fol-
   lowing higher-order library functions on lists.

   a. Decide if all elements of a list satisfy a predicate:

        all :: (a -> Bool) -> [a] -> Bool

   b. Decide if any element of a list satisﬁes a predicate:
    
        any :: (a -> Bool) -> [Bool] -> Bool

   c. Select elements from a list while they satisfy a predicate:

        takeWhile :: (a -> Bool) -> [a] -> [a]

   d. Remove elements from a list while they satisfy a predicate:

        dropWhile :: (a -> Bool) -> [a] -> [a]

   Note: in the prelude the first two of these functions are generic functions
   rather than being speciﬁc to the type of lists.
-}

-- 2.a
all' :: (a -> Bool) -> [a] -> Bool
all' f []     = True
all' f (x:xs) = (f x) && (all' f xs)

-- 2.b
any' :: (a -> Bool) -> [a] -> Bool
any' f []     = False
any' f (x:xs) = (f x) || (any' f xs)

-- 2.c
takeWhile' :: (a -> Bool) -> [a] -> [a]
takeWhile' f [] = []
takeWhile' f (x:xs)
    | f x       = x : (takeWhile' f xs)
    | otherwise = []

-- 2.d
dropWhile' :: (a -> Bool) -> [a] -> [a]
dropWhile' f [] = []
dropWhile' f (x:xs)
    | f x       = dropWhile' f xs
    | otherwise = x : xs

{-
3. Redefine the functions map f and filter p using foldr. 
-}

map' :: (a -> b) -> [a] -> [b]
map' f [] = []
map' f xs = foldr (\y ys -> (f y) : ys) [] xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' p [] = []
filter' p xs = foldr (\y ys -> if p y then y : ys else ys) []  xs 

{-
4. Using foldl, define a function dec2int :: [Int] -> Int that converts a
   decimal number into an integer. For example:

   > dec2int [2,3,4,5]
   2345
-}

--dec2int :: [Int] -> Int
--dec2int [] = 0
--dec2int xs = foldl (\y z -> y + z) 0 xs

{-
5. Without looking at the definitions from the standard prelude, define the
   higher-order library function curry that converts a function on pairs into a
   curried function, and, conversely, the function uncurry that converts a curried
   function with two arguments into a function on pairs.

   Hint: first write down the types of the two functions.
-}

curry' :: ((a, b) -> c) -> a -> b -> c
curry' f = (\x -> (\y -> f (x, y)))

uncurry' :: (a -> b -> c) -> ((a, b) -> c)
uncurry' f = \(x, y) -> f x y

{-
6. A higher-order function unfold that encapsulates a simple pattern of
   recursion for producing a list can be define as follows:

        unfold p h t x
            | p x = [] 
            | otherwise = h x : unfold p h t (t x)

   That is, the function unfold p h t produces the empty list if the predicate
   p is true of the argument value, and otherwise produces a non-empty list by
   applying the function h to this value to give the head, and the function t to
   generate another argument that is recursively processed in the same way to
   produce the tail of the list. For example, the function int2bin can be
   rewritten more compactly using unfold as follows:

        int2bin = unfold (== 0) (‘mod‘ 2) (‘div‘ 2)

    Redefine the functions chop8, map f and iterate f using unfold.
-}

unfold :: (a -> Bool) -> (a -> b) -> (a -> a) -> a -> [b]
unfold p h t x 
    | p x       = []
    | otherwise = h x : unfold p h t (t x)

type Bit = Int

int2bin :: Int -> [Bit]
int2bin = unfold (== 0) (`mod` 2) (`div` 2)

-- chop encoded string into a list of 8-bit binary numbers.
chop8 :: [Bit] -> [[Bit]]
chop8 = unfold (== []) (take 8) (drop 8)

mapu :: Ord a => (a -> b) -> [a] -> [b]
mapu f = unfold (== []) (f . head) (tail)

-- I couldn't come up with anything for iterate' :). Apparently the following
-- is the solution, which I woudn't have been able to work out by myself tbh ¯\_(ツ)_/¯.
iterate' :: (a -> a) -> a -> [a]
iterate' = unfold (const False) id

{-
7. Modify the binary string transmitter example to detect simple transmission
   errors using the concept of parity bits. That is, each eight-bit binary number
   produced during encoding is extended with a parity bit, set to one if the number
   contains an odd number of ones, and to zero otherwise. In turn, each resulting
   nine-bit binary number consumed during decoding is checked to ensure that its
   parity bit is correct, with the parity bit being discarded if this is the case,
   and a parity error being reported otherwise.

   Hint: the library function error :: String -> a displays the given string as
   an error message and terminates the program; the polymorphic result type
   ensures that error can be used in any context.
-}

-- This part uses functions from part 6.

-- convert from binary to integer.
bin2int :: [Bit] -> Int
bin2int = foldr (\x y -> x + 2*y) 0

-- ensure binary number is 8 bits.
make8 :: [Bit] -> [Bit]
make8 bits = take 8 (bits ++ repeat 0)

-- encode string into a list of bits.
encode :: String -> [Bit]
encode = concat . map (make8 . int2bin . ord)

-- encode string into a list of bits, and pad it
-- with a parity bit.
encodeWithParityBit :: String -> [Bit]
encodeWithParityBit s = encoded ++ [parityBit]
  where
    encoded   = encode s
    parityBit = (sum encoded) `mod` 2

decode :: [Bit] -> String
decode = map (chr . bin2int) . chop8

-- check if list of bits has even parity.
evenParity :: [Bit] -> Bool
evenParity xs = last xs == (sum (init xs) `mod` 2)

-- decode bits containing parity bit. Unless even parity is met,
-- report parity error.
decodeWithParityBit :: [Bit] -> String
decodeWithParityBit bits
    | evenParity bits = decode (init bits)
    | otherwise       = error "Unmatched parity bit"

{-
8. Test your new string transmitter program from the previous exercise using a
   faulty communication channel that forgets the first bit, which can be modelled
   using the tail function on lists of bits.
-}

faultyChannel :: [Bit] -> [Bit]
faultyChannel = tail

transmit :: String -> String
transmit = decodeWithParityBit . faultyChannel . encodeWithParityBit

{-
9. Define a function altMap :: (a -> b) -> (a -> b) -> [a] -> [b] that
   alternately applies its two argument functions to successive elements in a list,
   in turn about order. For example:

   > altMap (+10) (+100) [0,1,2,3,4]
    [10,101,12,103,14]
-}

-- altMap :: (a -> b) -> (a -> b) -> [a] -> [b]
-- altMap f g []       = []
-- altMap f g [x]      = [f x]
-- altMap f g (x:y:xs) = (f x) : (g y) : altMap f g xs

altMap :: (a -> b) -> (a -> b) -> [a] -> [b]
altMap _ _ [] = []
altMap f g (x:xs) = f x : altMap g f xs

{-
10. Using altMap, define a function luhn :: [Int] -> Bool that implements the
    Luhn algorithm from the exercises in chapter 4 for bank card numbers of any
    length. Test your new function using your own bank card.
-}

luhnDouble :: Int -> Int
luhnDouble digit
    | result > 9 = result - 9
    | otherwise  = result
  where
    result = 2 * digit

checkDigit xs = 10 - unit
  where
    unit = sum (altMap luhnDouble id (drop 1 (reverse xs))) `mod` 10

luhn :: [Int] -> Bool
luhn xs = (last xs) == (checkDigit xs)

