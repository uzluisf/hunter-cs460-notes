
The exercises in this directory are from Graham Hutton's [*Programming in Haskell
(2nd Edition)*](https://books.google.com/books/about/Programming_in_Haskell.html?id=75C5DAAAQBAJ).

Please keep in mind that the solutions might not be the most efficient and/or most
idiomatic way to do it in Haskell.
