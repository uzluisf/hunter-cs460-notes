-- 01. Introduction

{-
1. Give another possible calculation for the result of `double (double 2)`.

double (double 2)
= double (2 + 2)
= 4 + 4
= 8
-}


{-
2. Show that `sum [x]  = x` for any number `x`.

sum [x]
sum x:[]
x + sum []
x + 0
x
-}
 
{-
3. Define a function `product` that produces the product of a list of numbers, and
   show using your definition that `product [2,3,4] = 24`.
-}

prod        :: [Int] -> Int
prod []     = 1
prod (x:xs) = x * prod xs

{-
prod [2, 3, 4]
= 2 * prod [3, 4]
= 2 * 3 * prod [4]
= 2 * 3 * 4
= 24
-}

{-
4. How should the definition of the function `qsort` be modified so that it
   produces a `reverse` sorted version of a list?
   
By switching up the sublists `smaller` and `greater` during the concatenation
with the list containing the pivot. Thus:

qsortr []     = []
qsortr (x:xs) =
    qsortr greater ++ [x] ++ qsortr smaller
  where
    smaller = [a | a <- xs, a <= x]
    greater = [b | b <- xs, b > x]
-}

{-
5. What would be the effect of replacing <= by < in the original definition
   of qsort? Hint: consider the example qsort [2,2,3,1,1]

qsort [2,2,3,1,1]
= qsort [1] ++ [2] ++ qsort [3]
= ([] ++ [1] ++ []) ++ [2] + ([] ++ [3] ++ [])
= [1] ++ [2] ++ [3]
= [1, 2, 3]

Duplicate elements are removed.
-}

