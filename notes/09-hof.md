
# Higher-order Functions

## Processing Lists with `map` and `filter`

The standard prelude defines a number of useful higher-order functions for pro-
cessing lists. Many of these are actually generic functions that can be used
with a range of different types. In this section, we'll discuss two functions:

* the function `map`, which applies a function to all elements of a list.
  Defined using a list comprehension is as follows:

  ```haskell
  map :: (a -> b) -> [a] -> [b]
  map f xs = [f x | x <- xs]

  map :: (a -> b) -> [a] -> [b]
  map f []     = []
  map f (x:xs) = f x : map f xs
  ```

  That is, `map f xs` returns the list of all values `f x` such that `x` is an
  element of the argument list `xs`. For example:

  ```haskell
  > map (+1) [1,2,3,4]
  [2,3,4,5]

  > map (\x -> (x * x)) [1,2,3,4]
  [1,4,9,16]

  > map reverse ["abc","def"]
  ["cba", "fed"]
  ```

  Three points to note about `map`:

  1. It's a polimorphic function that can be applied to lists of any type.
  2. It can be applied to itself to process nested lists. For example, the
     function `map (map (1+))` increments each number in a list of lists of
     numbers:

     ```haskell
     map (map (+1)) [[1, 2],[3]]
     [map (+1) [1, 2], map (1+) [3]]
     [[2,3],[4]]
     ```
   3. It can be defined using recursion:

      ```haskell
      map :: (a -> b) -> [a] -> [b]
      map f []     = []
      map f (x:xs) = f x  : map f xs
      ```

* the function `filter`, which selects all elements of a list that satisty a
  function known as the predicate (or property) which returns a logical value.
  Using a list comprehension, `filter` is defined as follows:

  ```haskell
  filter :: (a -> Bool) -> [a] -> [a]
  filter p xs = [x | x <- xs, px]
  ```

  That is, `filter p xs` returns the list of all values `x` such that `x` is an
  element of the list `xs` and the value `p x` is `True`. For example:

  ```haskell
  > filter even [1..10]
  [2,4,6,8,10]
  
  > filter (\x -> (x `mod` 2 /= 0)) [1..10]
  [1,3,5,7,9]

  > filter (>5) [1..10]
  [6,7,8,9,10]
  ```

The functions `map` and `filter ` are often used together in programs, with
`filter` being used to select certain elements from a list, each of which is
then transformed using `map`. For example, a function that returns the sum of
the squares of the even integers from a list could be defined as follows:

```haskell
sumsqreven :: [Int] -> Int
sumsqreven ns = sum (map (^2) (filter even ns))
```

Some other higher-order functions for processing lists defined in the standard
prelude are:

* `all` - decide if all elements of a list satisfy a predicate.

  ```haskell
  > all even [2,4,6,8]
  True
  > all even [2,4,6,9]
  False
  ```

* `any` - decide if any element of a list satisfies a predicate.

  ```haskell
  > any odd [2,4,6,8]
  False
  > all odd [2,4,6,9]
  True
  ```

* `takeWhile` - selects elements from a list while they satisfy a predicate.

  ```haskell
  > takeWhile even [2,4,6,9]
  [2,4,6]
  ```

* `dropWhile` - remove elements from a list while they satisfy a predicate.

  ```haskell
  > dropWhile even [2,4,6,9]
  [9]
  ```

## The `foldr` Function

The `foldr` (abbreviating *fold right*) function reduces a list of values into a
single value using an operator, all which is encapsulated by the following
pattern:

```
f []     = v
f (x:xs) = x # f xs
```

That is, the function maps the empty list to a value `v`, and any non-empty list
to an operator `#` applied to the head of the list and the result of recursively
processing the tail. For example, the `sum` and `product` functions can be
defined using this pattern of recursion:

```haskell
sum []     = 0
sum (x:xs) = x + sum xs

product []     = 1
product (x:xs) = x * product xs
```

which can be rewritten more compactly by simply using `foldr` as follows:

```haskell
sum :: Num a => [a] -> a
sum xs = foldr (+) 0 xs

product :: Num a => [a] -> a
product xs = foldr (*) 1 xs
```

These definitions could be made simpler by making the list argument implicit
using partial application:


```haskell
sum :: Num a => [a] -> a
sum = foldr (+) 0

product :: Num a => [a] -> a
product = foldr (*) 1
```

The `foldr` function itself can be defined using recursion:

```haskell
foldr :: (a -> b -> b) -> b -> [a] -> b
fold f v []     = v
fold f v (x:xs) = f x (foldr f v xs)
```

That is, the function `foldr f v` maps the empty list to the value v, and any
non-empty list to the function `f` applied to the head of the list and the
recursively processed tail. In practice, however, it is best to think of the
behaviour of `foldr f v` in a non-recursive manner, as simply replacing each cons
operator in a list by the functon `f`, and the empty list at the end by the
value `v`. For example,

```haskell
foldr (+) 0 1 : (2 : (3 : []))
```

gives the result


```haskell
1 + (2 + (3 + 0))
```

The name *fold right* reflects the use of an operator with  **right
associativity**. More generally the behavior of `foldr` can be summarized as
follows:

```haskell
foldr (#) v [x0,x1,...,xn] = x0 # (x1 # (... (xn # v) ...))
```

## The `foldl` Function
  
Similar to `foldr`, the function `foldl` (for *fold left*) reduces a list of
values into a single using an operator. However, the operator is assumed to be
**left associativity**. 

The functions `sum` and `product` can defined in this this manner by using
auxiliary functions `sum'` and `product'` respectively that takes an extra
argument `v` that is used to accumulate the final result:

```haskell
sum :: Num a => [a] -> a
sum = sum' 0
  where
    sum' v []     = v
    sum' v (x:xs) = sum' (v+x) xs

product :: Num a => [a] -> a
product = product' 1
  where
    product' v []     = v
    product' v (x:xs) = product' (v*x) xs
```

All of this is described by the following pattern of recursion, which is
encapsulated by the function `foldl`:

```haskell
f v []     = v
f v (x:xs) = f (v # x) xs
```

The functions `sum` and `product` can be written more compactly using `foldl`:

```haskell
sum :: Num a => [a] -> a
sum = foldl (+) 0

product :: Num a => [a] -> a
product = foldl (*) 1
```

Note that `sum` and `product` can be defined using both `foldr` and `foldl`. The
choice of which definition is preferable is usually made on grounds of efficiecy
and requires careful consideration of the evaluaton mechanism underlying
Haskell.

The `foldl` function itself can defined using recursion:

```haskell
foldl :: (a -> b -> a) a -> [b] -> a
foldl f v []    = v
foldl f v (x:xs) = foldl f (f v x) xs
```


As with `foldr` it's beest to think of the behavior of `foldl` in a
non-recursive manner, in terms of an operator `#` that's **left associative** by
replacing each cons operator in a list by the function `f` and the empty list at
the start by the value `v`. For example.

```haskell
foldl (+) 0 1 : (2 : (3 : []))
```

gives the result

```haskell
((0 + 1) + 2) + 3
```

More generally, this behavior is summarized as follows:

```haskell
foldl (#) v [x0,x1,...,xn] = (... ((v # x0) # x1) ...) # xn
```

![Illustration of left and right folding](./images/fold-left-and-right.png)

## The Composition Operator

The higher-order library operator `.` returns the composition of two functions
as a single function, and can be defined as follows:

```haskell
(.) :: (b -> c) -> (a -> b) -> (a -> c)
f . g = \x f ( g x)
```

That is, `f . g`, which is read `f composed with g`, is the function that takes
an argument x, applies the function `g` to `x`, and applies the function `f` to
the result. This operator can be alternatively defined as `(f . g) x = f (g x)`.

Function composition can be used to simplify nested function applications, by
reducing parentheses and avoiding the need to explicitly refer to the initial
argument. For example, the following function definitions

```haskell
odd n = not (even n)

twice f x  = f (f x)

sumsqreven ns = sum (map (^2) (filter even ns))
```

can be rewritten more simply:

```haskell
odd = not . even

twice f x  = f . f

sumsqreven ns = sum . map (^2) . filter even
```

Composition also has an identity, given by the identity function:

```haskell
id :: a -> a
id = \x -> x
```

The identity function is often useful when reasoning about programs, and also
provides a suitable starting point for a sequence of compositions. For example,
the composition of a list of functions can be defined as follows:

```haskell
compose :: [a -> a] -> (a -> a)
compose = foldr (.) id
```

For example:

```haskell
inc :: Num a => a -> a
inc = \x -> (x + 1)

double :: Num a => a -> a
double = \x -> (x * 2)

succ-double :: Num a => a -> a
succ-double = compose [inc, double]

succ-double 5 -- #=> 11
```
