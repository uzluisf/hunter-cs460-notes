
# Intro to Functional Programming

The functional programming paradigm, which is based on mathematical functions,
is the design basis of the most important nonimperative styles of languages.
This style of programming is supported by functional programming languages.

## Mathematical Functions

In mathematics, a **function** is a mapping from a set $A$ into a set $B$ such
that each element of $A$ is mapped into a unique element of $B$. The set $A$ (on
which the function $f$ is defined) is called the **domain** of $f$. The set of
all elements of $B$ mapped to elements of $A$ by $f$ is called the **range** (or
**codomain**) of $f$, and is denoted by $f(A)$.

If $f$ is a function from $A$ into $B$, then we write:

\begin{align*}
f : A \rightarrow B
\end{align*}

We also write the equation $f(a) = b$ to mean that the *value* (or result) from
applying function $f$ to an element $a \in A$ is an element $b \in B$.

A function $f : A \rightarrow B$ is:

* **injective** (or one-to-one) if and only if distinct elements of $A$ are
  mapped to distinct elements of $B$. In other words, $f(a) = f(a')$ iff $a =
  a'$.

* **surjective** (or onto) if and only if, for every element $b \in B$, there's
  some element $a \in A$ such that $f(a) = b$.

* **bijection** (or one-to-one correspondence) if and only if $f$ is one-to-one
  and onto.

Given functions $f : A \rightarrow B$ and $g : B \rightarrow C$, the
**composition** of $f$ and $g$, written $g \circ f$, is function $A$ and $C$
such that

\begin{align*}
(g \circ f)(a) = g(f(a))
\end{align*}

A function $f^{-1} : B \rightarrow A$ is an *inverse* of $f : A \rightarrow B$
if and only if, for every $a \in A$, $f^{-1}(f(a)) = a$. An inverse exists for
any one-to-one function.

If a function $f : A \rightarrow B$ and $A \subseteq A'$, then we say that $f$
is a **partial function** from $A'$ to $B$ and a *total function* from $A$ to $B$.
In other words, there are some elements of $A'$ on which $f$ may be undefined.

A function $\oplus : (A \times A) \rightarrow A$ is called a **binary operation**
on $A$. We usually write binary operations in infix form: $a \oplus a'$. In
Computer Science, a function $\oplus : (A \times B) \rightarrow C$ is also
called a binary operation.

Let $\oplus$ be a binary operarion on some set $A$ and $x$, $y$, and $z$ be
elements of $A$. Then:

* Operation $\oplus$ is commutative iff $x \oplus y = y \oplus x$ for
  any $x$ and $y$.

* Operation $\oplus$ is commutative iff $(x \oplus y) \oplus z = x
  (y \oplus z)$ for any $x$, $y$, and $z$.

* An element $e$ of a set $A$ is a *left identity* of $\oplus$ iff $e \oplus x =
  x$ for any $x$, a *right identity* iff $x \oplus e = x$m and an *identity* iff
  it's both a left and right identity. An identity of an operation is sometimes
  called a *unit* of the operation.

* An element $z$ of a set $A$ is *left zero$ of $\oplus$ iff $z \oplus x = z$
  for any $x$, a *right zero* iff $x \oplus z =z$, and a *zero* iff it's both a
  right and left zero.

* If $e$ is the identity of $\oplus$ and $x \oplus y = e$ for some $x$ and $y$,
  then $x$ if a *left inverse* of $y$ and *y* is a a *right inverse* of $x$.
  Elements $x$ and $y$ are inverses of each if $x \oplus y = e = y \oplus x$.

* If $\oplus$ is an *associative* operation, then $\oplus$ and $A$ are said to
  form a *semigroup*.

* A semigroup that also has an *identity* element is called a *monoid*.

* If every element of a monoid has an inverse then the monoid is called a
  *group*.

* If a monoid or group is also commutative, then it's said to be *Abelian*.

## Function Definitions

**Note:** Mathematicians usually refer to the set of non-zero positive integers
as the **natural numbers**. Computing scientists usually include $0$ in the set
of natural numbers.

Consider the factorial *fact*. This function can be defined in several ways. For
any natural number, we might define *fact* with

* a product 

$$fact(n) = 1 \times 2 \times 3 \times \cdots \times n $$

* using the product operator

$$fact(n) = \prod^{i=n}_{i = 1} i$$

* a *recursive* definition 

\begin{align*}
fact'(n) =
\left\{
	\begin{array}{ll}
		1,                   & \mbox{if } n = 0 \\
		n \times fact'(n-1), & \mbox{if } n \geq 0
	\end{array}
\right.
\end{align*}

## Mathematical Induction over Natural Numbers

To prove a proposition $P(n)$ holds for any natural number $n$, one must show
two things:

* **Base case:** $n = 0$. That $P(0)$ holds.

* **Inductive case:** $n = m + 1$. That, if $P(m)$ holds for some natural number
  $m$, then $P(m + 1)$ also holds. The $P(m)$ assumption is known as the
  *induction hypothesis*.

Example: Let's prove that the two definitions of $fact$ and $fact'$ are
quivalent for all natural numbers $n$, that is,

\begin{align*}
fact(n) = fact'(n)\text{.}
\end{align*}

**Base case:** $n = 0$
\begin{align*}
fact(0) &= \prod^{i=n}_{i = 0} \\
        &= 1 \quad \text{(empty range for $\prod$, $1$ is product's identity element)} \\
        &= fact'(0) \quad \text{(definition of $fact'$ when $n = 0$)}
\end{align*}

**Inductive case:** $n = m + 1$
\begin{align*}
fact(m+1) &= \prod^{i = m + 1}_{i = 1} \\
          &= (m + 1) \times \prod^{i = m}_{i = 1} \\
          &= (m + 1) \times fact(m)  \quad \text{(induction hypothesis)}\\
          &= (m + 1) \times fact'(m) \quad  \text{(definition of $fact()'$ for $m + 1 > 0$)}\\
          &= fact'(m + 1)
\end{align*}

Therefore, we've proved $fact(n) = fact'(n)$ for all natural numbers $n$. QED

## Functions and Lambda Calculus

### Function Definition and Evaluation

Function definitions are often written as a function name, followed by a list of
parameters in parentheses, followed by the mapping expression. For example,

\begin{align*}
cube(x) &= x^{3}, \text{where $x$ is a real number} \\
incr(y) &= y + 1, \text{where $y$ is an integer} \\
\end{align*}

**Note:** Sebesta uses $\equiv$ for function definition. This text will stick
with $=$ as used in mathematics for the same purpose. Thus, $f(n) \equiv n
\times n$ and $f(n) = n \times n$ are the same.

In the definition of the function $cube$, the parameter $x$ can represent any
number of the domain set, but it's fixed to represent one specific element
during **evaluation** of the function expression.

**Function applications** are specified by pairing the function name with a
particular element of the domain set. The range element is obtained by
evaluating the function-mapping expression with the domain element substituted
for the occurrences of the parameter. Note that during evaluation, the mapping
of a function contains no **unbound parameters**, where a **bound parameter** is
a name for a particular value. Every occurrence of a parameter is bound to a
value from the domain set and is a constant during evaluation. 

For example, consider the following evaluation of $cube(x)$:

\begin{align*}
cube(2.0) = 2.0 \times 2.0 \times 2.0 = 8.0
\end{align*}

The parameter $x$ is bound to $2.0$ during the evaluation and there no unbound
parameters. Furthermore, $x$ is a constant and thus its value cannot be changed
during evaluation.

### Lambda Calculus

**Lambda calculus** (also written as $\lambda$-calculus) is a formal system in
mathematical logic for expressing computation based on function abstraction and
application using variable binding and substitution. It was introduced by the
mathematician **Alonzo Church** in the 1930s as part of his research into the
foundations of mathematics.

At heart, lambda calculus is a simple notation for functions and application.
The main ideas are applying a function to an argument and forming functions by
abstraction. The syntax of basic $\lambda$-calculus is quite sparse, making it an
elegant, focused notation for representing functions. Functions and arguments
are on a par with one another. Its sparse syntax doesn't hinder
$\lambda$-calculus; its the expressiveness and flexibility makes it a cornucopia
of logic and mathematics.

Lambda calculus is defined on the basis of **lambda expressions**, which specify
the parameters and the mapping of functions. The lamba expression is the
function itself, which is **nameless**. For example, consider the following 
lambda expression:

\begin{align*}
\lambda(x).x^{3}
\end{align*}

We can *apply* this function on another expression (a variable, or another
function), like

\begin{align*}
(\lambda(x).x^{3})(2)
\end{align*}

which results in the value $8$.

## Characteristics of Functional Programming

### Higher-Order Functions

A **higher-order function** is a function that either takes one or more
functions as arguments, returns a function as its result, or both.

Higher order functions allow us to compose functions, i.e., **function
composition**. This means we can write small functions and combine them to
create larger functions, just like putting a bunch of small LEGO bricks
together to build a house.

For instance, let $f = x + 2$, $g = 3 * x$, and $h = f \circ g$ its function
composition, then $h$ is defined as 

\begin{align*}
h = f(g(x)) = (3 * x) + 2
\end{align*}

### First-Class Functions

Functions are first-class if they can be treated as values. They can be assigned
as values, passed into functions, and returned from functions.

Although most imperative languages don't support first class functions, some
languages such as Perl, Python, Raku, etc. do treat function as first-class
objects. 

### Apply-to-All

**Apply-to-all** is a functional form that takes a single function as a
parameter. If applied to a list of paramenets, apply-to-all applies its
functional parameter to each of the values in the list parameter and collects
the results in a list or sequence. Apply-to-all is denoted by $\alpha$.

For example, let $h(x) = x^{2}$, then

\begin{align*}
\alpha(h, (2, 3, 4))
\end{align*}

yields $(4, 9, 16)$. The function $h$ is applied to each element of the list
and thus square it, and the new list is returned.

In programming languages, *apply-to-all* are known as *map* functions. 

### Referential Transparency

An expression is **referentially transparent** if it always yields the same value
whenever it is evaluated. Any functions that are called as part of the
expression evaluation must be **pure functions**.

Pure functions must not depend upon the state of the running program (for
example, the values of global variables) or input from an I/O device. In
addition, pure functions must not modify state information or output to an
I/O device. In other words, these functions cause no *side effects*. The
result of the function should only be dependent upon its arguments. 

The importance of referential transparency is that

* it allows programmers to reason about program behavior, for example, in
  proving program correctness and debugging code.
* it assists the optimizing compiler in eliminating the recalculation of
  expressions when it is known that a recalculation will always produce the
  same result.

Most programming languages, even functional languages, do not guarantee that
expressions in the language are referentially transparent because of the
incorporation of imperative style features such as assignment statements.
However, the functions in the languages [Haskell](https://www.haskell.org/) and
[Miranda](https://www.cs.kent.ac.uk/people/staff/dat/miranda/) are pure and all
functional languages can be used in mostly a purely functional style.

### Implicit Parametric Polymorphism

Polymorphic subroutines are those that can work with multiple types of data.
Many programming languages support either parametric or subtype polymorphism. In
**explicit parametric polymorphism**, code, such as a class, takes a parameter
that specifies the type of data for which the code is customized. Explicit
parametric polymorphism is named **generics** in Java, Ada, Eiffel, etc.

Many functional languages, such as Lisp, Haskell, etc., provide **implicit
parametric polymorphism**. Functions in the language can operate on different
types of data, but without the programmer being required to indicate the type
upon which the function will operate.

Note that implicit parametric polymorphism is not the same as function
overloading. In function overloading, multiple functions with the same name and
the same number of parameters (but with different types) provide similar
functionality but on different types of data. With parametric polymorphism, there
is only one function that can be invoked using different types of data.

### List Types

Recursion and higher order functions (e.g., apply-to-all functions) are the
bread and butter of functional programming. These mechanisms are well suited
for use with list structures and most functional programming languages provide a
list type and many built-in list operations. 

## Support for Functional Programming in Imperative Languages

### Python

Python’s lambda expressions define simple one-statement anonymous functions. The
syntax of a lambda expression in Python is exemplified by the following: 

```python
lambda a, b : 2 * a - b
```

Python includes the higher-order function `filter` and `map`:

```python
map(lambda x: x ** 3, [2, 4, 6, 8]) #=> [8, 64, 216, 512]
```

Python also supports partial function applications by importing `partial` from the
`functools` module. For example:

```python
from functools import partial
basetwo = partial(int, base=2)
basethree = partial(int, base=3)

print(basetwo('10010'))   #=> 18
print(basethree('10010')) #=> 84
```

### Raku

Raku is a multi-paradigmatic programming language that provides a wealth of
functional programming primitives, lazy and eager list evaluation, junctions,
autothreading and hyperoperators (vector operators).

An anonymous function is defined in Raku with the same syntax as a regular
function (i.e., `sub name(parameters) { ... }`), except that the name of the
function is omitted.

```raku
map sub (\a) { a ** 3 }, (2, 4, 6, 8); #=> (8 64 216 512)
```

For small-scale reuse, Raku provides blocks which are syntactically a list of
statements enclosed in curly braces.

```raku
map {$_ ** 3 }, (2, 3, 6, 8); #=> (8, 64 216 512)
```

Raku also supports partial function application via the `assuming` method. In
the following example, `parse-base` is a builtin subroutine that converts a
string with base-`$radix` to its numeric equivalent (e.g., `parse-base('FF',
16)` returns the integer $255$):

```raku
my &base-two = &parse-base.assuming(*, 2);
my &base-three = &parse-base.assuming(*, 3);

say basetwo '10010';    #=> 18
say basethree '10010';  #=> 84
```

### Ruby

Ruby’s blocks are effectively subprograms that are sent to methods, which makes
the method a higher-order subprogram. A Ruby block can be converted to a
subprogram object with **lambda**. 

```ruby
base = lambda {|base, num| num.to_i(base) }
basetwo = base.curry.(2)
basethree = base.curry.(3)

puts basetwo.('10010')   #=> 18
puts basethree.('10010') #=> 84
```
