
# Haskell Types and Classes

A **type** is a collection of related values. The following are examples of
types:

* the type `Bool`, which contains the two logical values `False` and `True`.
* the type `Bool -> Bool` contains all functions that map arguments from `Bool` to
  results from `Bool`, such as the logical negation function `not`. 

We use the notation $v :: T$ to mean that $v$ is a value in the type $T$, and
say that $v$ *has type* $T$. For example, `False :: Bool`, `'A' :: Char`, etc.

Because Haskell is a purely functional language, all computations are done via
the evaluation of expressions (syntactic terms) to yield values (abstract
entities that we regard as answers). Every expression must have a type, which is
calculated prior to evaluating the expression by a process called **type
inference**. The following simple typing rule for function application
encapsulate the process: 

$$
\frac{f :: A \rightarrow B \quad e :: A}{f \; e :: B}
$$

This rule states that if $f$ is a function that maps arguments of type $A$ to
results of type $B$, and $e$ is an expression of type $A$, then the application
$f\; e$ has type $B$. For example, consider the function `isEven` defined as
follows:

```haskell
isEven :: Int -> Bool
isEven n = n `mod` 2 == 0
```

which maps from `Int` to `Bool` and the expression `6` of type `Int`. Then, the
typing of the expression $isEven\; 6$ can be inferred from the fact that `isEven ::
Int -> Bool` and `6 :: Int`. On the other hand, the expression `isEven 'Z'`
doesn't have a type under the above rule, because this would require that `'Z'
:: Int`, which isn't valid since `'A'` isn't an integer value.

## Basic Types

* `Bool` for logical values. It contains the two logical values `True` and
  `False`.

* `Char` for single characters. It contains all single characters in the Unicode
  system enclosed in single quotes. For example, `'a'`, `'3'`, `'_'`, `'\n'`
  (newline), etc. are values of type `Char`.

* `String` for strings of characters. It contains all sequences of characters
  enclosed in double quotes. For example, `"abc"`, `"123"`, `"1 + 2 = 3"`, `""`
  (the empty string), etc. are values of type `String`.

* `Int` for fixed-precision integers. The GHC system has values of type `Int` in
  the range -$2^{63}$ to $2^{63}-1$. For example, `-100`, `0`, `99`, etc. are
  values of type `Int`.

  Note that going outside this range can give unexpected results. For example,
  evaluating `2^63 :: Int` gives a negative number, which is incorrect. Here,
  we're using `::` to force the result to be an `Int` rather than other numeric
  type.

* `Integer` for arbitrary-precision integers .It contains all integers, with as
  much memory as necessary being used for their storage. thus avoiding the
  imposition of lower and upper bounds on the range of numbers. For example,
  evaluating `2^63 :: Integer` produces the correct result.

  Apart from the different memory requirements and precision for numbers of
  type `Int` and `Integer`, the choice between these two types is also one of
  performance. Most computers have built-in hardware for fixed-precision
  integers, whereas arbitrary-precision integers are usually processed using
  the slower medium of software, as sequences of digits.

* `Float` for single-precision floating-point numbers. It contains decimal
  numbers with a fixed amount of memory being used for their storage. For
  example, `-12.34`, `1.0`, `3.1415927`, etc. are values of type `Float`.

* `Double` for double-precision floating-point numbers. Similar to `Float`,
  except that twice as much memory is used for storage of these numbers to
  increase their precision.

## List Types

A *list* is a sequence of elements of the same type, with the elements being
enclosed in square parentheses and separated by commas. We write `[T]` for the
type of all lists whose elements have type `T`. For example, `[False, True] ::
Bool`, `[1,2,3] :: Int`, `['a', 'e', 'i', 'o', 'u'] :: Char`, `[['a'], ['b']] ::
[[Char]]`, etc.

The number of elements in a list is called its *length*. The list `[]` of length
zero is called the empty list. Note that `[[]]` and `[]` are different lists,
the former being a singleton list comprising the empty list as its only element,
and the lattter being simply the empty list that has no elements.

The list `[False,True]` in Haskell is actually shorthand for the list
`False:(True:[])` (`:` is the infix operator that adds its first argument to the
front of its second argument). Since `:` is right associative, we can also write
this list as `False:True:[]`.

There is no restriction that a list must have a finite length. In particular,
due to the use of lazy evaluation in Haskell, lists with an infinite length are
both natural and practical.

## Tuple Types

A *tuple* is a finite sequence of components of possibly different types, with the
components being enclosed in round parentheses and separated by commas. We
write $(T_1,T_2, \ldots,T_n)$ for the type of all tuples whose $i_{th}$
components have type $T_i$ for any $i$ in the range $1$ to $n$. For example,
`(False,True) :: (Bool,Bool)`, `(1,'A) :: (Int,Char)`, etc.

The number of components in a tuple is called its **arity**. The tuple `()` of
arity zero is called the *empty tuple*, tuples of arity two are called *pairs*,
tuples of arity three are called *triples*, and so on. Tuples of arity one, such
as `(False)`, are not permitted because they would conflict with the use of
parentheses to make the evaluation order explicit, such as in `(1+2)*3`.

## Polymorphic Types

Haskell also incorporates *polymorphic types*, i.e., types that are universally
quantified in some way over all types. Polymorphic type expressions essentially
describe families of types. [Stack Overflow](https://stackoverflow.com/a/14299983/10824322)
discussion about universally vs. existential quantified types.

For example, `[a]` is the family of types consisting of, for every type `a`, the
type of lists of `a`. Lists of integers (e.g. `[1,2,3]`), lists of characters
(e.g., `['a','b','c']`), lists of lists of integers (e.g., `[[1], [1,2]]`),
etc., are all members of this family. Note, however, that `[2,'b']` is not a
valid example, since there is no single type that contains both `2` and `'b'`.

The library function `length` calculates the length of any list, irrespective of
the type of the elements of the list. For example:

```haskell
> length [1, 2, 3]
3
> length ["Yes", "No"]
2
> length []
0
```

The idea that `length` can be applied to lists whose elements have any type is
made precise in its type by the inclusion of a **type variable**. Type variables
must begin with a lowercase letter, and are usually simply named `a`, `b`, `c`,
and so on. Type variables are uncapitalized to distinguish them from specific
types such as `Int`. The type of `length` is as follows:

```haskell
length :: [a] -> Int
```

That is, for any type `a`, the function `length` has type `[a] -> Int`. A type
that contains one or more type variables is called *polymorphic*. Hence, `[a] ->
Int` is a polymorphic type and `length` is a polymorphic function. More
generally, many of the functions provided in the standard prelude are
polymorphic. For example:

```haskell
fst :: (a,b) -> a
head :: [a] -> a
take :: Int -> [a] -> [a]
id :: a -> a
```

## Overloaded Types

The type of the arithmetic operator `+` is as follows:

```haskell
(+) :: Num a => a -> a -> a
```

The `Num a =>` part in the type signature is known as a **class constraint** and
means that `+` can be applied to numbers of any numeric type. Class constraints
are written in the form `C a`, where `C` is the name of a class and `a` is a
type variable. Thus, more precisely, the type signature of `+` states that for
any type `a` that is an **instance** of the class `Num` of numeric types, the
function `(+)` has type `a -> a -> a`. 

A type that contains one or more class constraints is called **overloaded**.
Hence, `Num a => a -> a -> a` is an overloaded type and `(+)` is an overloaded
function. More generally, most of the numeric functions provided in the prelude
are overloaded. For example:

```haskell
(*) :: Num a => a -> a -> a
negate :: Num a => a -> a
abs :: Num a => a -> a
```

Numbers themselves are also overloaded. For example, `3 :: Num a => a` means
that for any numeric type `a`, the value `3` has type `a`. In this manner, the
value `3` could be an integer, a floating-point number, or more generally a
value of any numeric type, depending on the context in which it's used.

**NOTE:** Since class constraints must be applied to function signatures that
already contain at least a type variable, an overloaded type has to be
polymorphic. 

## Basic Classes

Recall that a type is a collection of related values. Building upon this notion,
a *class* is a collection of types that support certain overloaded operations
called *methods*. Haskell provides a number of basic classes that are built-in
to the language, of which the most commonly used are described below.

* `Eq` for equality types. This class contains types whose values can be
  compared for equality and inequality using the following two methods:

  ```haskell
  (==) :: a -> a -> Bool
  (/=) :: a -> a -> Bool
  ```

  All the basic types `Bool`, `Char`, `String`, `Int`, `Integer`, `Float`, and
  `Double` are instances of the `Eq` class, as are lists and tuple types,
  provided that their element and component types are instances for equality
  comparable types.

  Note that function types aren't in general instances of the `Eq` class,
  because it's not feasible in general to compare two functions for equality.

* `Ord` for ordered types. This class contains types that are instances of the
  equality class `Eq`, but in addition whose values are totally ordered, and as
  such can be compared and processed using the following six methods:

  ```haskell
  (<) :: a -> a -> Bool
  (<=) :: a -> a -> Bool
  (>) :: a -> a -> Bool
  (>=) :: a -> a -> Bool
  min :: a -> a -> a
  max :: a -> a -> a
  ```

  Like with `Eq`, all the basic types are instances of the `Ord` class, as are
  list types and tuple types, provided that their element and component types
  are instances.

* `Show` for showable types. This class contains types whose values can be
  converted into strings of characters using the following method:

  ```haskell
  show :: a -> String
  ```

  All the basic types `Bool`, `Char`, `String`, `Int`, `Integer`, `Float`, and
  `Double` are instances of the `Show` class, as are lists and tuple types,
  provided that their element and component types are instances for equality
  comparable types.

* `Read` for readable types. This class is dual to `Show`, and contains types
  whose values can be converted from strings of characters using the following
  method:

  ```haskell
  read :: String -> a
  ```

  All the basic types `Bool`, `Char`, `String`, `Int`, `Integer`, `Float`, and
  `Double` are instances of the `Read` class, as are lists and tuple types,
  provided that their element and component types are instances for equality
  comparable types.

* `Num` for numeric types. This class contains types whose values are numeric,
  and as such can be processed using the following six methods:

  ```haskell
  (+) :: a -> a -> a
  (-) :: a -> a -> a
  (*) :: a -> a -> a
  negate :: a -> a
  abs :: a -> a
  signum :: a -> a
  ```

  The the basic types `Int`, `Integer`, `Float`, and `Double` are instances of
  the `Num` class.

  Note that the `Num` class doesn't provide a division method; division is
  handled separately by two special classes, one for integral numbers and one
  for fractional numbers.

* `Integral` for integral types. This class contains types that are instances of
  the numeric class `Num`, but in addition whose values are integers, and as
  such support the methods of integers division and integer remainder.

  ```haskell
  div :: a -> a -> a
  mod :: a -> a -> a
  ```

  The basic types `Int` and `Integer` are instances of the `Integral` class.

* `Fractional` for fractional types. This class contains types that are
  instances of the numeric class `Num`, but in addition whose values are
  non-integral, and as such support the methods of fractional division and
  fractional reciprocation.

  ```haskell
  (/) :: a -> a -> a
  recip :: a -> a
  ```
  
  The basic types `Float` and `Double` are instances of the `Fractional` class.

## Type Declarations

Declaring a new type with the `type` keyword introduces a new name for an
existing type in Haskell. For example, the type `String` is simply a synonym for
the type `[Char]`, which is a list of characters:

```haskell
type String = [Char]
```

Type declarations can be nested, in the sense that one such type can bde
declared in terms of another. For example, we might declare a type `Point2D` as
a pair of integers, and a transformation as a function  on positions:

```haskell
type Point2D = (Int, Int)
type Trans = Point2D -> Point2D
```

**Type declarations cannot be recursive**. For example, the following definition
for a tree which is a pair comprising an integer and a list of subtrees:

```haskell
type Tree = (Int, [Tree])
```

If required, recursive types can be declared using the more powerful `data`
mechanism, discussed in [@sec:data-declarations].

Type declarations can also be parameterized by other types. For example, we
could define the type `Pair` for functions that manipulate pairs of values of
the same type:

```haskell
type Pair a = (a,a)
```

Type declarations with more than one parameter are possible too. For example, a
type of lookup tables that associate keys of one type to values of another type
can declared as a list of `(key,value)` pairs:

```haskell
type Assoc k v = [(k,v)]
```

This type can be used to declare a function that returns the first value that's
associated with a given key in a table:

```haskell
find :: Eq k => k -> Assoc k v -> v
find k t = head [v | (k',v) <- t, k == k']

find 'z' [(i,j) | (i, j) <- zip ['a'..'z'] [0..25]]
-- => 25
```

## Data Declarations {#sec:data-declarations}

Contrary to `type`, declaring a type with the `data` keyword doesn't introduce
a synonym for an existing type but a new type of its own. For example, the type
`Bool` from the standard prelude comprises two new values, named `False` and
`True`:

```haskell
data Bool = False | True
```

In such declarations, the symbol $\vert$ is read as *or*, and the new values of
the type are called *constructors* (e.g., `False` and `True` are constructors of
the type `Bool`).

There are a few points to highlight here:

* both the type and its constructors must begin with a capital letter; this is
  enforced by the Haskell compiler to distinguish them from function names which
  must begin with a lowercase letter.

* the same constructor name cannot be used in more than one type.

* names given to new types and constructors have **no inherent meaning** to the
  Haskell system; the meaning are assigned by the programmer via the functions
  that they define on new types. For example, the declaration of the type `Bool`
  can be equally written as `data A = B | C`.

Values of new types in Haskell can be used in precisely the same way as those of
built-in types. In particular, they can freely be passed as arguments to
functions, returned as results from functions, stored in data structures, and
used in patterns. For example, given the declaration for a type `Move`, we can
declare a few functions that make use of it:

```haskell
type Pos  = (Int, Int)
data Move = North | East | South | West

-- move Pos instance to one of Move cases.
move :: Move -> Pos -> Pos
move North (x,y) = (x,y+1)
move East  (x,y) = (x+1,y)
move South (x,y) = (x,y-1)
move West  (x,y) = (x-1,y)

-- perform a number of moves.
moves :: [Move] -> Pos -> Pos
moves []     p = p
moves (m:ms) p = moves ms (move m p)
```

The constructors in a data declaration can also have arguments. For example,
consider the type `Shape` for shapes that comprise circles with a given radius
and rectangles with width and height:

```haskell
data Shape = Circle Float | Rect Float Float

-- create a square of a given size.
square :: Float -> Shape
square n = Rect n n

-- find area of shapes.
area :: Shape -> Float
area (Circle r) = pi * r^2
area (Rect w h) = w * h
```

Because of their use of arguments, the constructors `Circle` and `Rect` are
actualy *constructor functions*. Unlike normal functions, constructor functions
are already fully evaluated and cannot be further simplified, because there are
no definition equations for them (e.g., `Circle`). 

```haskell
> :type Circle
Circle :: Float -> Shape
```

Data declarations themselves can be parameterized. Consider the datatype `Maybe`
from the standard prelude:

```haskell
data Maybe a = Nothing | Just a
```

That is, a value of type `Maybe a` is either `Nothing` or of the form `Just x`
for some value `x` of type `a`. For example, consider a safe version of the
library function `div`, which returns `Nothing` (for division-by-zero error) and
`Just x` otherwise. Here, `Nothing` represents failure while `Just` represents
success.

```haskell
safediv :: Int -> Int -> Maybe Int
safediv _ 0 = Nothing
safediv m n = Just (m `div` n)
```

## Newtype Declarations

The keyword `newtype` is used whenever a new type has a single constructor with
a single argument. For example, a type of natural numbers (non-negative
integers) could be declared as follows:

```haskell
newtype Nat = N Int
```

where the single constructor `N` takes a single argument of type `Int`, which is
up tp the programmer to ensure that this is always non-negative. How does this
newtype declaration from a type and data declaration of `Nat`?

```haskell
type Nat = Int
data Nat = N Int
```

There are a few differences:

* Using `newtype` rather than `type` means that `Nat` and `Int` are different
  types rather than synonyms, and hence the type system of Haskell ensures that
  they cannot be accidentally be mixed in a program.

* Using `newtype` rather than `data` brings an efficiency benefit, because
  `newtype` constructors don't incur any cost when programs are evaluated, as
  they're automatically removed from the compiler once type checking is
  completed.

## Recursive Types

New types declared using the `data` and `newtype` declarations can also be
recursive. For example, the type of natural numbers can be declared recursively:

```haskell
data Nat = Zero | Succ Nat
```

which states that a value of type `Nat` is either `Zero`, or the form `Succ n`
for some value `n` of type `Nat`. Here, `Zero` represents the number $0$, and
`Succ` represents the sucessor function `(1+)`. For example, `Succ (Succ Zero)`
represents `1 + (1 + 0) = 2`. We can define the following functions to convert
from `Nat` to `Int` and vice versa:

```haskell
nat2int :: Nat -> Int
nat2int Zero     = 0
nat2int (Succ n) = 1 + nat2int n

int2nat :: Int -> Nat
int2nat 0 = Zero
int2nat n = Succ (int2nat (n-1))
```

## Class and Instance Declarations

In Haskell, a new class can be declared using the `class` mechanism. For
example, the class `Eq` of equality types is decared in the standard prelude
as follows:

```haskell
class Eq a where
    (==), (/=) :: a -> a -> Bool

    x /= y = not (x == y)
```

This declaration states that for a type `a` to be an instance of the class `Eq`,
it must support equality and inequality operators of the specified type. In
fact, because a *default definition* has already been included for the `/=`
operator, declaring an instance only requires a definition for the `==` operator.

For example, a type `TrafficLight` can be made into equality and show type as
follows:

```haskell
data TrafficLight = Red | Green | Yellow

instance Eq TrafficLight where
    (Red)    == (Red)    = True
    (Green)  == (Green)  = True
    (Yellow) == (Yellow) = True
    _        == _        = False

instance Show TrafficLight where
    show Red    = "Don't proceed."
    show Yellow = "Prepare to stop."
    show Green  = "Proceed."
```

As a result, you can do the following:

```haskell
> Red == Red
True

> Red == Green
False

> show Red
"Don't proceed."
```

Note that only types declared with `data` and `newtype` can be made into
instances of classes. Furthermore, default definitions can be overriden in
instance declarations if desired (e.g., for some equality types it's more
efficient to decide if two values are different than simply checking if they
aren't equal). 

Classes can also be extended to form new classes. For example, the class `Ord`
of types whose values are totally ordered is declared in the standard prelude as
an extension of the class `Eq` as follows:

```haskell
class Eq a => Ord a where
    (<), (<=), (>), (>=) :: a -> a -> Bool
    min, max             :: a -> a -> a

    min x y | x <= y    = x
            | otherwise = y

    max x y | x <= y    = y
            | otherwise = x
```

This states that for a type to be an instance of `Ord` it must be an instance of
`Eq`, and support six additional operators.

## Derived Instances

In Haskell, newly declared types can be made into instances of a number of
built-in classes (`Eq`, `Ord`, `Show`, and `Read`) by using the `deriving`
mechanism. For example, the type `Bool` is actually declared in the standard
prelude as follows:

```haskell
data Bool = False | True
    deriving (Eq, Ord, Show, Read)
```

Now all the member functions from the four derived classes can then be used with
logical values:

```haskell
> False == False
True

> False < True
True

> show False
"False"

> read "False" :: Bool
False
```

In the case of constructors with arguments, the types of these arguments must
also be instances of any derived classes. For example, recall the following
declaration:

```haskell
data Shape   = Circle Float | Rect Float Float
```

To derive `Shape` as an equality type requires that the type `Float` is also an
equality type, which is indeed the class. 

