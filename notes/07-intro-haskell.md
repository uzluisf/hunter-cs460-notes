
# Intro to Haskell

## A Taste of Haskell

Let's make our first Haskell program an implementation of the factorial function
for natural numbers. Remember that we define the natural numbers to consist of
all non-negative integer numbers, which mean we include $0$.

We defined the factorial function $fact$ as follows:

\begin{align*}
fact(n) = \prod^{i = n}_{i = 1} \text{ (Note that $fact(0) = 1$, which is multiplication's identity element.)}
\end{align*}

We also defined the factorial function with a recursive definition:

\begin{align*}
fact'(n) =
\left\{
	\begin{array}{ll}
		1,                   & \mbox{if } n = 0 \\
		n \times fact'(n-1), & \mbox{if } n \geq 0
	\end{array}
\right.
\end{align*}

One way to translate the recursive defintion $fact'$ (we call it `factRec)` into
Haskell is the following:

```{#lst:factRec .haskell caption="Recursive implementation of factorial using if-then-else."}
-- Recursive implementation of factorial in Haskell
factRec :: Int -> Int
factRec n = if n == 0 then
                1
            else
                n * factRec (n-1)
```

There are a few things to notice in the previous Haskell code snippet:

* **Type signature.** The line `factRec :: Int -> Int` is the **type signature**
  for function `factRec`. In general, type signatures have the syntax `object ::
  type`.

  Here object `factRec` is defined as a function (denoted by the `->` symbol)
  that takes one argument of type integer (first `Int`) and returns a value of
  type integer (last `Int`). You can read `::` as "has type". Thus, `factRec` is
  an object that has type `Int -> Int`.

  Haskell does not have a built-in natural number type. Thus we choose type `Int`
  for the argument and result of `factRec` (`Int` is a bounded integer type
  guaranteed to have at least the range $[-2^{29}, 2^{29}-1]$, i.e., fixed-precision
  integers). Haskell also has the unbounded integer `Integer` which is limited
  only by the amount of memory on your machine, i.e., arbitrary-precision
  integers.

* **Function declaration.** The declaration for the function `factRec` begins on
  the second line. Note that it's an equation of the form `fname params = body`
  where `fname` is the function's name, `params` are the function's parameters,
  and `body` is an expression defining the function's result.

  A function may have zero or more parameters. The parameters are listed after
  the function name without being enclosed in parentheses and without commas
  separating them.

  The parameter identifiers may appear in the body of the function. In the
  evaluation of a function application the actual argument values are
  substituted for parameters in the body.

* **Expression.** The function `factRec` is defined to be an `if-then-else`
  expression. Evaluation of the `if-then-else` yields the value $1$ if
  argument `n` has the value $0$ and the value `n * factRec (n-1)` otherwise.

  The `else` clause includes a recursive application of
  `factRec`. The expression `(n-1)` is the argument for the recursive
  application. For each recursive application of `factRec` to a natural number,
  the argument's value moves closer to the base case, i.e., `n = 1`.

* **Indentation.** Unlike most conventional languages, the *indentation is
  significant* in Haskell. The indentation (or *layout rule*) makes it possible
  to determine the grouping of deﬁnitions from their indentation. 

* **Comments.** In addition to new definitions, Haskell code can also contain
  comments that will be ignored by the compiler. Haskell supports two kinds of
  comments, called *ordinary* and *nested*. Ordinary comments begin with the
  symbol `--` and extend to the end of the line. Nested comments are enclosed
  within `{-` and `-}`.

Let's ignore the fact the function `factRec` does not match the mathematical
definition given above. The domains of the function include also negative
integers, which means the evaluation of `factRec` will go into an “infinite loop”
and eventually abort when it is applied to a negative value.

## Guards

A **guard** is simply a boolean expression that must evaluate to true if the
program execution is to continue in the branch in question. In a sequence of
guards, whichever guard matches first (in the order listed) becomes the
definition of the function for the particular application (other guards might
match as well, but they are not used for a call if listed later).

An alternative way to differentiate the two cases in the recursive definition of
the factorial function is to use a different equation for each case. If the
`Boolean` guard (e.g., `n == 0`) for an equation evaluates to true, then that
equation is used in the evaluation of the function.

```{#lst:factGd .haskell caption="Recursive implementation of factorial using guards."}
factGd :: Int -> Int
factGd n
  | n == 0    = 1
  | otherwise = n * factGd (n-1)
```

The function `factGd` is equivalent to `factRec`. The guards are evaluated in a
top-to-bottom order. The `otherwise` guard, which is simply defined as
`otherwise = True` catches everything, succeeds if the `n == 0` guard
fails; thus it is similar to the trailing `else` clause on the `if-then-else`
expression used in `factRec`.

We mentioned before that `factRec` didn't match the mathematical definition of
the factorial function since it allows for negative numbers, which causes the
evaluation to go into an infinite loop for negative arguments. To avoid this,
we can remove the negative integers from the function’s domain. One way to do
this is by using guards to narrow the domain to the natural numbers as in the
following definition of `factGdPos`:

```{#lst:factGdPos .haskell caption="Recursive implementation of factorial using guards."}
factGdPos :: Int -> Int
factGdPos n 
  | n == 0 = 1
  | n >= 1 = n * factGdPos (n-1)
```

Function `factGdPos` is undefined for negative arguments. If `factGdPos` is
applied to a negative argument, the evaluation of the program encounters an
error quickly and returns without going into an infinite loop.

## Pattern Matching

In functional programming, we are "more concerned with how something is defined
than with the specifics of how it is calculated". The idea is that it is a
compiler or interpreter's job to figure out how to reach a solution, not the
programmer's.

One useful way of specifying how a function is defined is to describe what
results it will return given different types of inputs. A powerful way of
describing "different types of inputs" in Haskell is using **pattern matching**.
We can provide multiple definitions of a function, each having a particular
pattern for input arguments.

An alternative to differentiate the two cases in the recursive defintion of the
factorial function is to use a different equation for each case:

```{#lst:factPm .haskell caption="Recursive implementation of factorial using pattern matching."}
factPm :: Int -> Int
factPm 0 = 1
factPm n = n * factPm (n-1)
```

The parameter pattern $0$ in the first leg of the definition only matches
arguments with value $0$. Since Haskell checks patterns and guards in a
top-to-bottom order, the `n` pattern matches all nonzero values. Thus `factRec`,
`factGd`, and `factPm` are equivalent.

## Builtin function

All the previous definitions of the factorial function use patterns similar to
the recurrence relation $fact'$. Another alternative is to use the library
function `product` and the list-generating `[1..n]` to defined a solution that's
the function $fact$:

```{#lst:factProd .haskell caption="Recursive implementation of factorial using builtin function."}
factProd :: Int -> Int
factProd n = product [1..n]
```

The list expression `[1..n]` generates a list of consecutive integers beginning
with $1$ and ending with `n`. The library function `product` computes the product of
the elements of this finite list. If `factProd` is applied to a negative
argument, it will return the value $1$. This is consistent with the function
$fact$ upon which is based.

## Function Application

In Haskell there is only one way to form more complex expressions from simpler
ones, and that's by using **function application**. Neither parentheses nor
special operator symbols are used to denote function application, which is
denoted by simply listing the argument expressions following the function's
name. For example, in mathematics the expression 

$$
f(a, b) + cd
$$

means apply the function $f$ to two arguments $a$ and $b$, and add the result to
the product of $c$ and $d$. Reﬂecting its central status in the language, function
application in Haskell is denoted silently using spacing, while the
multiplication of two values is denoted explicitly using the operator $*$. Thus,
the expression above in Haskell is:

```haskell
f a b + c * d
```

Function application (i.e., juxtaposition of function names and argument
expressions) has higher precedence than other operators. For example, 
`f a + b` means `(f a) + b` rather than `f (a + b)`.

Now consider this definition of a function `add` which adds its two arguments:

```haskell
add :: Int -> Int -> Int
add x y = x + y
```

Unlike the previous definitions of the factorial function where the type of the
type signature was `Int -> Int`, the function `add`'s type is `Int -> Int ->
Int`. Just like `factRec`, for example, `add` is really just a function of one
argument. 

An application of `add` has the form `add e1 e2`, where `e1` and `e2` are
expressions, and is equivalent to `(add e1) e2`, since function application
associates to the left as explained above. In other words, applying `add` to one
argument yields a new function which is then applied to the second argument.
This is consistent with the type of `add`, `Int -> Int -> Int`, which is
equivalent to `Int -> (Int -> Int)`; i.e. `->` associates to the right. 

We can now define an `incr` function as follows:

```haskell
incr :: Int -> Int
incr n = n + 1
```

However, using `add`, we can define `inc` in a different way from earlier:

```haskell
incr :: Int -> Int
incr n = add n 1
```

The function `add` is an example of a *curried function* (The name *curry*
from Haskell Brooks Curry who popularized the idea. He's also the person the
Haskell programming language is named after, along two more other programming
languages, Curry and Brook). 

The definition of `incr` with `add` is an example of the **partial application**
of a curried function, and is one way in which a function can be returned as a value.
Let's consider a case in which it's useful to pass a function as an argument.
The well-known `map` function is a perfect example:

```{#lst:map-function .haskell caption="Definition of the builtin map function."}
map :: (a->b) -> [a] -> [b]
map f  []    =  []
map f (x:xs) =  f x : map f xs
```

Let's start by deciphering `map`'s type signature: The first argument
`(a->b)` denotes simply a function that takes some argument of type `a` and
returns something of type`b`. The second argument is `[a]`, which is a list of
values of type `a`, and the return type `[b]`, a list of values of type `b`. 
In plain english, the `map` function applies a function to each element in a
list of values, then returns the those values as a list.

We note `map` is defined using pattern matching. When the function is passed the
empty list `[]`, then it returns an empty list. Otherwise, take the first
element of the list and apply function `f` to it, and prepend the result
to the application of `map` to `f` and the remaining list `xs`. The **colon
operator** (`:`) simply prepends a single element to a list (and returns a new
list). Remember function application has higher precedence than any infix operator, and
thus the right-hand side of the second equation parses as `(f x) : (map f xs)`.

The `map` function is **polymorphic** ("many shapes"); this is given away by the
use of type variables (`a`, `b`, etc.) in the signature. As an example of the
use of `map`, we can increment the elements in a list:

```haskell
map (add 1) [1, 2 , 3]
```

Or using `incr`:

```haskell
map incr [1, 2 , 3]
```

These examples demonstrate the first-class nature of functions, which when used
in this way are usually called **higher-order functions** as discussed earlier.

## Standard Prelude

Haskell comes with a large number of built-in functions, which are defined in a
library file called the standard **prelude**; these functions are included
automatically into all Haskell modules. We've used some of them already; for
example, `+`, `product`, and `map`.

In addition to familiar numeric functions such as `+` and *, the prelude also
provides a range of useful functions that operate on lists. In Haskell, the
elements of a list are enclosed in square parentheses and are separated by
commas, as in `[1,2,3,4,5]`. 

There are several library functions that operate on list you should know about
(In the following examples, we're using `GHCi` which is the *Glasgow Haskell
Compiler* in interactive mode; hence the `>` which waits from the user's input
for evaluation):

* `!!` - select the $n^{th}$ from a list (lists are `0`-indexed).

  ```haskell
  > [1, 2, 3, 4, 5] !! 2
  3
  ```

* `head` - select the first element of a non-empty list.

  ```haskell
  > head [1, 2, 3, 4, 5]
  1
  ```

* `last` - return the last element from a finite and non-empty list.

  ```haskell
  > last [1, 2, 3, 4, 5]
  5
  ```

* `tail` - return all the elements from a list except the first one. 

  ```haskell
  > tail [1, 2, 3, 4, 5]
  [2,3,4,5]
  ```

* `init` - return all the elements of a non-empty list except the last one. 

  ```haskell
  > last [1, 2, 3, 4, 5]
  [1,2,3,4]
  ```

* `take` - select the first $n$ elements of a list.

  ```haskell
  > take 3 [1, 2, 3, 4, 5]
  [1,2,3]
  ```

* `drop` - return all the elements of a non-empty list except the first $n$
  elements.

  ```haskell
  > drop 3 [1, 2, 3, 4, 5]
  [4,5]
  ```

* `length` - return the length of a list.

  ```haskell
  > length [1, 2, 3, 4, 5]
  5
  ```

* `sum` - compute the sum of a list of numbers.

  ```haskell
  > sum [1, 2, 3, 4, 5]
  15
  ```

* `product` - compute the product of a list of numbers.

  ```haskell
  > product [1, 2, 3, 4, 5]
  120
  ```

* `++` - append two lists.

  ```haskell
  > [1, 2, 3] ++ [4, 5]
  [1,2,3,4,5]
  ```

* `reverse` - reverse a list.

  ```haskell
  > [1, 2, 3, 4, 5]
  [5,4,3,2,1]
  ```

## Haskell Scripts

In addition to the the functions provided in the standard prelude, it is also
possible to define new functions. New functions are defined in a `script`, a text
file comprising a sequence of definitions. By convention, Haskell scripts
usually have a `.hs` suffix on their filename to differentiate them from other
kinds of files.

Let's save the following Haskell function definitions in a file `arith.hs`:

```{#lst:script-example .haskell caption="Haskell script with few function definitions."}
-- return the sum of two integers.
square   :: Int -> Int
square n = n + n

-- return the sum of squares of two integers.
sumOfSquares     :: Int -> Int -> Int
sumOfSquares x y = square x + square y
```

By starting up the GHCi system and instructing to load the script `arith.hs`:

```bash
$ gchi arith.hs
```

the functions `square` and `sumOfSquares` can be readily used:

```
> square 6
36
> sumOfSquares 3 4
25
```

GHCi does not automatically reload scripts when they are modified, so a reload
command must be executed before the new definitions can be be used. For
example, if we added the function definition `factRec` from the previous
sections to `arith.hs`, we can make it available as follows:

```haskell
> :reload

> factRec 6
720
```

There are many commands available from CHCi's command prompt. [@tbl:ghci-commands-tbl]
summarises the meaning of some of the most commonly used GHCi commands:

|Command | Meaning |
|:-------|:--------|
| `:load name`       | load script `name` |
| `:reload`          | reload current script |
| `:set editor name` | set editor to `name` (e.g., `nano`, `vi`, `vim`, etc.) |
| `:edit name`       | edit scrip `name` |
| `:edit`            | edit current script |
| `:type expr`       | show type of `expr` |
| `:?`               | show all commands |
| `:quit`            | quit GHCi |

: GHCi commands {#tbl:ghci-commands-tbl}
