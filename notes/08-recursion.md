
# List Comprehensions and Recursion in Haskell

## List Comprehensions

In mathematics, the **comprehension** notation can be used to construct new sets
from existing sets. For example, the comprehension $\{x^2 \; | \; x \in \{1..5\}\}$
produces the set $\{1, 4, 9, 16, 25\}$ of all numbers $x^2$ such that $x$ is an
element of the set $\{1..5\}$. In Haskell a similar notation can be used to
construct lists from existing lists. 

For example:

```haskell
> [x^2 | x <- [1..5]]
[1,4,9,16,25]
```

The symbol `|` is read *such that*, `<-` is read as *is drawn from*, and the
expression `x <- [1..5]` is called a **generator**. Note that a list comprehension
can have more than one generator, with successive generators being separated by
commas. 

For example, the list of all possible pairings of an element from the list `[1,
2, 3]` with an element from the list `[4, 5]` can be produced as follows:

```haskell
> [(x,y) | x <- [1..3], y <- [4..5]]
[(1,4),(1,5),(2,4),(2,5),(3,4),(3,5)]
```

The following list comprehension defines the function `flatten` that flattens a
list of lists by using one generator to select each list in turn, and another to
select each element from each list:

```haskell
flatten :: [[a]] -> [a]
flatten xss = [x | xs < xss, x <- xs]
```

The **wildcard pattern** `_` is sometimes useful in generators to discard certain
elements from a list. For example, a function that selects all the first
components from a list of pairs can be defined as follows:

```haskell
firsts :: [(a,b)] -> [a]
firsts ps = [x | (x, _) <- ps]
```

List comprehensions can also use logical expressions called **guards** to filterh
the values produced by earlier generators. If a guard is `True`, then the
current values are retained; if it is `False`, then they are discarded. For
example:

```haskell
factors :: Int -> [Int]
factors n = [x | x <- [1..n], n `mod` x == 0]
```

defines the function `factors` that maps a positive integer to a list of its
positive factors.

`String`s aren't primitive in Haskell; they're constructed from lists of
characters (i.e., `[Char]`). Since strings are lists, any polymorphic function
on lists can be used with strings too. For example, a function `lowers` that
returns the number of lowercase letters that occur in a string can be defined as
follows:

```haskell
lowers :: String -> Int
lowers xs = length [x | x <- xs, x >= 'a' && x <= 'z']
```

## Recursion

In Haskell, functions can be defined in terms of themselves. Such functions are
called **recursive**. For example, the factorial function is defined as follows
recursively using pattern matching:

```haskell
fact :: Int -> Int
fact 0 = 1
fact n = n * fact(n-1)
```

The equations `fact 0  = 1` and `fact n =  n * fact(n-1)` constitute the *base
case* and *recursive case* respectively.

Functions can also be defined using multiple recursion, in which a function is
applied more than once in its own definition. For example, a function that
calculates the $n^{th}$ Fibonacci number for any integer $n \ge 0$ can be
defined using double recursion as follows:

```haskell
fib :: Int -> Int
fib 0 = 1
fib 1 = 1
fib n = fib (n-2) + fib (n-1)
```

### Recursion on Lists

Recursion is not restricted to functions on integers, but can also be used to
define functions on lists. For example, the library function `product` can be
defined as follows:

```haskell
product :: Num a => a -> a
product []     = 1
product (x:xs) = x * product xs
```

The first equation states that the product of the empty list of numbers is one,
which is appropriate because one is the identity for multiplication. The second
equation states that the product of any non-empty list is given by multiplying
the first number and the product of the remaining list.

Functions with multiple arguments can also be defined using recursion on more
than one argument at the same time. For example, the library function `zip` that
takes two lists and produces a list of pairs can be defined as follows:

```haskell
zip :: [a] -> [b] -> [(a,b)]
zip [] _ = []
zip _ [] = []
zip (x:xs) (y:ys) = (x,y) : zip xs ys
```

### Why Recursion is Useful

* Some functions can naturally be defined using recursion. For example, the
  factorial and fibonacci functions are examples of these functions.

* Properties of functions defined using recursion can be proved using the
  mathematical technique of **induction**.

### More Examples

The following are few example of recursive functions applied on lists:

* `length`: The length of an empty list is $0$, and the length of any non-empty
  list is the successor of the length of its tail.

  ```haskell
  length :: [a] -> Int
  length []     = 0
  length (_:xs) = 1 + length xs
  ```

* `reverse`: The reverse of an empty list is simply the empty list, and the
  reverse of any non-empty list is given by appending the reverse of its tail
  and a singleton list comprising the head of the list.

  ```haskell
  reverse :: [a] -> [a]
  reverse []     = []
  reverse (x:xs) = reverse xs ++ [x]
  ```

* `sum`: The sum of the empty list is $0$, and the sum of any non-empty list is
  given by adding the head of the list to the sum of its tail.

  ```haskell
  sum :: Num a => [a] -> a
  sum []     = 0
  sum (x:xs) = x + sum xs
  ```

* `last`: The last element of a singleton list is its element, and the last of
  any non-singleton list is the last element of its tail.

  ```haskell
  last :: [a] -> a
  last [x]    = x
  last (_:xs) = last xs
  ```

* `flatten`: The flattening of an empty list is the empty list, and the
  flattening of a non-empty list is the head of the list concatenated to the
  flattening of its tail.

  ```haskell
  flatten :: [[a]] -> [a]
  flatten []     = []
  flatten (x:xs) = x ++ flatten xs
  ```

* `union`: The union of an empty set with a non-empty (or vice versa) is the
  non-empty set. Otherwise, if the first set's head is in the second set,
  then the union of the first set's tail with the second set. Else, concatenate
  the first's head to the union of the first's tail with the second set.

  ```haskell
  member :: Eq a => a -> [a] -> Bool
  member n [] = False
  member n (x:xs)
      | n == x    = True
      | otherwise = member n xs
  
  union :: Eq a => [a] -> [a] -> [a]
  union [] ys = ys
  union xs [] = xs
  union (x:xs) ys
      | member x ys = union xs ys
      | otherwise   = x : union xs ys
  ```

* `insert`: The insertion of an element `x` into the empty lists gives a
  singleton list, and its insertion into a non-empty list depends upon the
  ordering of the new element `x` and the head `y` of the list. If `x <= y`,
  then the new element `x` is simply prepended to the start of the list,
  otherwise the head `y` becomes the first element of the resulting list, and
  then we proceed to insert the new element into the tail of the given list.

  ```haskell
  insert :: Ord a => a -> [a] -> [a]
  insert x []     = [x]
  insert x (y:ys)
      | x <= y    = x : y : ys
      | otherwise = y : insert x ys
  ```

